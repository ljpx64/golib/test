package test

import "fmt"

// Result is produced by implementations of Evaluator.  It is the result of
// evaluating a provided value, x, against the logic of the evaluator.
type Result struct {
	Pass    bool
	Message string
	Summary map[string]string
}

// AssertPass asserts that the result has a value of Pass for true.  Otherwise,
// it fails the provided TLike.
func (r *Result) AssertPass(t TLike) {
	t.Helper()

	if r.Pass {
		return
	}

	failureMessage := fmt.Sprintf("\n\n░▒▓███ %v ███▓▒░\n", t.Name())
	failureMessage += fmt.Sprintf("%v\n\n", r.Message)

	for k, v := range r.Summary {
		failureMessage += fmt.Sprintf("%v: %v\n", k, v)
	}

	failureMessage += "\n"
	t.Fatalf(failureMessage)
}
