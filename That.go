package test

// That passes the provided value to the evaluator and asserts it passed
// evaluation.
func That(t TLike, x interface{}, y Evaluator) {
	t.Helper()
	y.Evaluate(x).AssertPass(t)
}
