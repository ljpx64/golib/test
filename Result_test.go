package test

import (
	"strings"
	"testing"
)

func TestResultAssertPassedSuccess(t *testing.T) {
	// Arrange.
	tMock := NewTMock()
	result := &Result{
		Pass: true,
	}

	// Act.
	result.AssertPass(tMock)

	// Assert.
	if tMock.DidFail() {
		t.Fatalf("was not expecting failure, but failed with '%v'", tMock.GetFailureMessage())
	}
}

func TestResultAssertPassedFailure(t *testing.T) {
	// Arrange.
	tMock := NewTMock()
	result := &Result{
		Pass: false,
		Summary: map[string]string{
			"x": "int",
			"y": "int",
			"s": "string",
		},
		Message: "x + y cannot equal a string",
	}

	// Act.
	result.AssertPass(tMock)

	// Assert.
	actual := tMock.GetFailureMessage()
	if !strings.Contains(actual, "\nx + y cannot equal a string\n\n") {
		t.Fatalf("failure string did not match: %v", actual)
	}
}
