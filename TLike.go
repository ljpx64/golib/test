package test

import "testing"

// TLike defines the methods of *testing.T and *testing.B that are used by this
// package.
type TLike interface {
	Fatalf(format string, args ...interface{})
	Helper()
	Name() string
}

var _ TLike = &testing.T{}
var _ TLike = &testing.B{}
