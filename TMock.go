package test

import (
	"fmt"
	"regexp"
)

// TMock is an implementation of TLike that is used to unit test the assertions
// in this package.  It likely has few uses outside of this package.
type TMock struct {
	failureMessage string
}

var _ TLike = &TMock{}

// NewTMock creates and returns a new, empty TMock.
func NewTMock() *TMock {
	return &TMock{}
}

// Fatalf simply formats the provided format and args and then stores the result
// to later be verified.  It can be searched for within the TMock using the
// DidFailWith method.
func (t *TMock) Fatalf(format string, args ...interface{}) {
	if t.failureMessage != "" {
		panic("cannot call Fatalf more than once")
	}

	t.failureMessage = fmt.Sprintf(format, args...)
}

// Helper simply does nothing.
func (t *TMock) Helper() {}

// Name always returns the empty string.
func (t *TMock) Name() string {
	return ""
}

// DidFail returns true if Fatalf was called at all.
func (t *TMock) DidFail() bool {
	return t.failureMessage != ""
}

// GetFailureMessage returns the failure message, or any empty string if Fatalf
// has not been called.
func (t *TMock) GetFailureMessage() string {
	return t.failureMessage
}

// DidFailWith returns true if Fatalf was called and the regex pattern provided
// matches the failure message.
func (t *TMock) DidFailWith(pattern string) bool {
	if t.failureMessage == "" {
		return false
	}

	rgx := regexp.MustCompile(pattern)
	return rgx.Match([]byte(t.failureMessage))
}
