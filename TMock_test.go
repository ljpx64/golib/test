package test

import "testing"

func TestTMockFatalfPopulatesFailureMessage(t *testing.T) {
	// Arrange.
	TMock := NewTMock()

	// Act.
	TMock.Fatalf("Hello, %v!", "World")

	// Assert.
	if TMock.failureMessage != "Hello, World!" {
		t.Fatalf("expected 'Hello, World!', actually '%v'", TMock.failureMessage)
	}
}

func TestTMockFatalfCanOnlyBeCalledOnce(t *testing.T) {
	// Arrange.
	TMock := NewTMock()
	TMock.Fatalf("Hello, %v!", "World")

	// Act.
	didPanic := func() (panicked bool) {
		panicked = false

		defer func() {
			if r := recover(); r != nil {
				panicked = true
			}
		}()

		TMock.Fatalf("Hello, %v!", "Again")
		return
	}()

	// Assert.
	if !didPanic {
		t.Fatalf("expected the call to panic")
	}
}

func TestTMockDidFailWithAlwaysReturnsFalseForEmptyString(t *testing.T) {
	// Arrange.
	TMock := NewTMock()

	// Act.
	matched := TMock.DidFailWith(``)

	// Assert.
	if matched {
		t.Fatalf("expected no matches")
	}
}

func TestTMockDidFail(t *testing.T) {
	// Arrange.
	TMock := NewTMock()

	// Act.
	failed1 := TMock.DidFail()
	TMock.Fatalf("Hello, %v!", "World")
	failed2 := TMock.DidFail()

	// Assert.
	if failed1 {
		t.Fatalf("reported true for didFail when hadn't failed")
		return
	}

	if !failed2 {
		t.Fatalf("did not report true for didFail when had failed")
	}
}

func TestTMockDidFailWithSuccess(t *testing.T) {
	// Arrange.
	TMock := NewTMock()
	TMock.Fatalf("expected 3, actually 4")

	// Act.
	matched := TMock.DidFailWith(`^expected \d, actually \d$`)

	// Assert.
	if !matched {
		t.Fatalf("expected a match")
	}
}

func TestTMockGetFailureMessage(t *testing.T) {
	// Arrange.
	TMock := NewTMock()
	TMock.Fatalf("Hello, %v!", "World")

	// Act.
	failureMessage := TMock.GetFailureMessage()

	// Assert.
	if failureMessage != "Hello, World!" {
		t.Fatalf("expected 'Hello, World!', actual '%v'", failureMessage)
	}
}

func TestTMockStubs(t *testing.T) {
	// Arrange.
	TMock := NewTMock()

	// Act.
	TMock.Helper()
	name := TMock.Name()

	// Assert.
	if name != "" || TMock.failureMessage != "" {
		t.Fatalf("expected stub calls to do nothing")
	}
}
