package has

import "testing"

func TestLength(t *testing.T) {
	testCases := []struct {
		x      interface{}
		y      int
		expect bool
	}{
		{x: []byte{}, y: 0, expect: true},
		{x: []byte{}, y: 1, expect: false},
		{x: map[int]int{0: 0}, y: 0, expect: false},
		{x: map[int]int{0: 0}, y: 1, expect: true},
		{x: "Hello, World!", y: 13, expect: true},
		{x: "Hello, World!", y: 14, expect: false},
	}

	for _, testCase := range testCases {
		result := Length(testCase.y).Evaluate(testCase.x)
		if result.Pass != testCase.expect {
			t.Fatalf("Expected %v result for x: %v and y: %v", testCase.expect, testCase.x, testCase.y)
		}
	}
}
