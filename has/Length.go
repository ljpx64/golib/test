package has

import (
	"fmt"
	"reflect"

	"gitlab.com/ljpx64/golib/test"
)

// LengthEvaluator determines if the provided value has and length and that it
// is equal to y.
type LengthEvaluator struct {
	y int
}

var _ test.Evaluator = &LengthEvaluator{}

// Length determines if the provided value has a length of y.
func Length(y int) *LengthEvaluator {
	return &LengthEvaluator{y: y}
}

// Evaluate evaluates the length of the provided x.
func (e *LengthEvaluator) Evaluate(x interface{}) *test.Result {
	xt := reflect.TypeOf(x)
	xk := xt.Kind()

	if xk != reflect.Array && xk != reflect.Chan && xk != reflect.Map && xk != reflect.Slice && xk != reflect.String {
		return &test.Result{
			Pass:    false,
			Message: "Expected a type with a length.",
			Summary: map[string]string{
				"x   ": fmt.Sprintf("%v", x),
				"type": fmt.Sprintf("%v", xt),
			},
		}
	}

	xl := reflect.ValueOf(x).Len()
	if xl != e.y {
		return &test.Result{
			Pass:    false,
			Message: fmt.Sprintf("Expected length of x to be %v but was %v.", e.y, xl),
			Summary: map[string]string{
				"x     ": fmt.Sprintf("%v", x),
				"type  ": fmt.Sprintf("%v", xt),
				"length": fmt.Sprintf("%v", xl),
			},
		}
	}

	return &test.Result{
		Pass: true,
	}
}
