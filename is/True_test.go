package is

import "testing"

func TestTrue(t *testing.T) {
	testCases := []struct {
		given  interface{}
		expect bool
	}{
		{given: true, expect: true},
		{given: false, expect: false},
		{given: 5, expect: false},
		{given: "Hello", expect: false},
	}

	for _, testCase := range testCases {
		result := True.Evaluate(testCase.given)

		if result.Pass != testCase.expect {
			t.Fatalf("expected result.Pass to be %v", testCase.expect)
		}
	}
}
