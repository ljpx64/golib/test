package is

import (
	"io"
	"testing"
)

func TestNil(t *testing.T) {
	testCases := []struct {
		given  interface{}
		expect bool
	}{
		{given: nil, expect: true},
		{given: []byte(nil), expect: false},
		{given: io.Writer(nil), expect: true},
	}

	for _, testCase := range testCases {
		result := Nil.Evaluate(testCase.given)

		if result.Pass != testCase.expect {
			t.Fatalf("expected result.Pass to be %v", testCase.expect)
		}
	}
}
