package is

import (
	"fmt"
	"reflect"

	"gitlab.com/ljpx64/golib/test"
)

// FalseEvaluator evaluates if the provided value is false.
type FalseEvaluator struct{}

var _ test.Evaluator = &FalseEvaluator{}

// False determines if the expression provided evaluates as the boolean false.
var False = &FalseEvaluator{}

// Evaluate evaluates the provided value to determine if it is false.
func (e *FalseEvaluator) Evaluate(x interface{}) *test.Result {
	val, ok := x.(bool)

	if !ok {
		return &test.Result{
			Pass:    false,
			Message: "Expected statement to be false, but was not a bool value.",
			Summary: map[string]string{
				"x   ": fmt.Sprintf("%v", x),
				"type": fmt.Sprintf("%v", reflect.TypeOf(x)),
			},
		}
	}

	if val {
		return &test.Result{
			Pass:    false,
			Message: "Expected statement to be false, but was true.",
		}
	}

	return &test.Result{
		Pass: true,
	}
}
