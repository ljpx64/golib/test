package is

import (
	"fmt"
	"reflect"

	"gitlab.com/ljpx64/golib/test"
)

// NilEvaluator evaluates if the provided value is nil.
type NilEvaluator struct{}

var _ test.Evaluator = &NilEvaluator{}

// Nil determines if the provided value is nil.  An interface that has a nil
// value but an associated type will not be considered nil.
var Nil = &NilEvaluator{}

// Evaluate evaluates the provided value to determine if it is nil.
func (e *NilEvaluator) Evaluate(x interface{}) *test.Result {
	if x != nil {
		return &test.Result{
			Pass:    false,
			Message: "Expected value to be nil, but was not nil.",
			Summary: map[string]string{
				"x   ": fmt.Sprintf("%v", x),
				"type": fmt.Sprintf("%v", reflect.TypeOf(x)),
			},
		}
	}

	return &test.Result{
		Pass: true,
	}
}
