package is

import (
	"io"
	"testing"
)

func TestNotNil(t *testing.T) {
	testCases := []struct {
		given  interface{}
		expect bool
	}{
		{given: nil, expect: false},
		{given: []byte(nil), expect: true},
		{given: io.Writer(nil), expect: false},
	}

	for _, testCase := range testCases {
		result := NotNil.Evaluate(testCase.given)

		if result.Pass != testCase.expect {
			t.Fatalf("expected result.Pass to be %v", testCase.expect)
		}
	}
}
