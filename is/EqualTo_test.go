package is

import (
	"testing"
)

func TestEqualTo(t *testing.T) {
	testCases := []struct {
		x      interface{}
		y      interface{}
		expect bool
	}{
		{x: 5, y: 5, expect: true},
		{x: 6, y: 5, expect: false},
		{x: int32(5), y: int64(5), expect: false},
		{x: []byte{1, 2, 3}, y: []byte{1, 2, 3}, expect: true},
		{x: []byte{}, y: []byte(nil), expect: false},
		{x: []byte(nil), y: nil, expect: false},
		{x: []byte(nil), y: []byte(nil), expect: true},
	}

	for _, testCase := range testCases {
		result := EqualTo(testCase.y).Evaluate(testCase.x)
		if result.Pass != testCase.expect {
			t.Fatalf("expected result.Pass to be %v for x: %v, y: %v", testCase.expect, testCase.x, testCase.y)
		}
	}
}
