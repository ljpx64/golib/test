package is

import (
	"fmt"
	"reflect"

	"gitlab.com/ljpx64/golib/test"
)

// EqualToEvaluator determines if two values are equal to each other.
type EqualToEvaluator struct {
	y interface{}
}

var _ test.Evaluator = &EqualToEvaluator{}

// EqualTo determines if two values are equal to each other as per the
// deep-equal rules in the reflect package.
func EqualTo(y interface{}) *EqualToEvaluator {
	return &EqualToEvaluator{y: y}
}

// Evaluate evaluates the provided x value against the y value stored in the
// evaluator to determine if they are equal.
func (e *EqualToEvaluator) Evaluate(x interface{}) *test.Result {
	xt := reflect.TypeOf(x)
	yt := reflect.TypeOf(e.y)

	if xt != yt {
		return &test.Result{
			Pass:    false,
			Message: "Expected values to have the same type.",
			Summary: map[string]string{
				"x     ": fmt.Sprintf("%v", x),
				"y     ": fmt.Sprintf("%v", e.y),
				"x type": fmt.Sprintf("%v", xt),
				"y type": fmt.Sprintf("%v", yt),
			},
		}
	}

	if !reflect.DeepEqual(x, e.y) {
		return &test.Result{
			Pass:    false,
			Message: "Expected values to be equal.",
			Summary: map[string]string{
				"x     ": fmt.Sprintf("%v", x),
				"y     ": fmt.Sprintf("%v", e.y),
				"x type": fmt.Sprintf("%v", xt),
				"y type": fmt.Sprintf("%v", yt),
			},
		}
	}

	return &test.Result{
		Pass: true,
	}
}
