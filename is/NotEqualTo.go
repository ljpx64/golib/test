package is

import (
	"fmt"
	"reflect"

	"gitlab.com/ljpx64/golib/test"
)

// NotEqualToEvaluator determines if two values are not equal to each other.
type NotEqualToEvaluator struct {
	y interface{}
}

var _ test.Evaluator = &NotEqualToEvaluator{}

// NotEqualTo determines if two values are not equal to each other as per the
// deep-equal rules in the reflect package.
func NotEqualTo(y interface{}) *NotEqualToEvaluator {
	return &NotEqualToEvaluator{y: y}
}

// Evaluate evaluates the provided x value against the y value stored in the
// evaluator to determine if they are not equal.
func (e *NotEqualToEvaluator) Evaluate(x interface{}) *test.Result {
	xt := reflect.TypeOf(x)
	yt := reflect.TypeOf(e.y)

	if xt != yt {
		return &test.Result{
			Pass: true,
		}
	}

	if reflect.DeepEqual(x, e.y) {
		return &test.Result{
			Pass:    false,
			Message: "Expected values to not be equal.",
			Summary: map[string]string{
				"x     ": fmt.Sprintf("%v", x),
				"y     ": fmt.Sprintf("%v", e.y),
				"x type": fmt.Sprintf("%v", xt),
				"y type": fmt.Sprintf("%v", yt),
			},
		}
	}

	return &test.Result{
		Pass: true,
	}
}
