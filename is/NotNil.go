package is

import (
	"fmt"
	"reflect"

	"gitlab.com/ljpx64/golib/test"
)

// NotNilEvaluator evaluates if the provided value is not nil.
type NotNilEvaluator struct{}

var _ test.Evaluator = &NotNilEvaluator{}

// NotNil determines if the provided value is not nil.  An interface that has a
// nil value but an associated type will be considered not nil.
var NotNil = &NotNilEvaluator{}

// Evaluate evaluates the provided value to determine if it is not nil.
func (e *NotNilEvaluator) Evaluate(x interface{}) *test.Result {
	if x == nil {
		return &test.Result{
			Pass:    false,
			Message: "Expected value to not be nil, but was.",
			Summary: map[string]string{
				"x   ": fmt.Sprintf("%v", x),
				"type": fmt.Sprintf("%v", reflect.TypeOf(x)),
			},
		}
	}

	return &test.Result{
		Pass: true,
	}
}
