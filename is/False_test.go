package is

import "testing"

func TestFalse(t *testing.T) {
	testCases := []struct {
		given  interface{}
		expect bool
	}{
		{given: true, expect: false},
		{given: false, expect: true},
		{given: 5, expect: false},
		{given: "Hello", expect: false},
	}

	for _, testCase := range testCases {
		result := False.Evaluate(testCase.given)

		if result.Pass != testCase.expect {
			t.Fatalf("expected %v to be %v", testCase.given, testCase.expect)
		}
	}
}
