package is

import (
	"fmt"
	"reflect"

	"gitlab.com/ljpx64/golib/test"
)

// TrueEvaluator evaluates if the provided value is true.
type TrueEvaluator struct{}

var _ test.Evaluator = &TrueEvaluator{}

// True determines if the expression provided evaluates as the boolean true.
var True = &TrueEvaluator{}

// Evaluate evaluates the provided value to determine if it is true.
func (e *TrueEvaluator) Evaluate(x interface{}) *test.Result {
	val, ok := x.(bool)

	if !ok {
		return &test.Result{
			Pass:    false,
			Message: "Expected statement to be true, but was not a bool value.",
			Summary: map[string]string{
				"x   ": fmt.Sprintf("%v", x),
				"type": fmt.Sprintf("%v", reflect.TypeOf(x)),
			},
		}
	}

	if !val {
		return &test.Result{
			Pass:    false,
			Message: "Expected statement to be true, but was false.",
		}
	}

	return &test.Result{
		Pass: true,
	}
}
