![](icon.png)

# test

Package `test` implements a fluid-like set of tools for performing assertions
in tests.

## Example Usage

```golang
func TestFabricOfTheUniverse(t *testing.T) {
	test.That(t, 2 + 2, is.EqualTo(4))
}
```

Icons made by [Dimitry Miroliubov](https://www.flaticon.com/free-icon/flask_639353?term=flask&page=3&position=64)
from [www.flaticon.com](https://www.flaticon.com/).