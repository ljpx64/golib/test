package test

// Evaluator defines the methods that any type capable of evaluating a provided
// value must implement.
type Evaluator interface {
	Evaluate(x interface{}) *Result
}
